terraform {
  required_version = "~> 1.3.0"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.7.0"
    }
    powerdns = {
      source  = "pan-net/powerdns"
      version = "1.5.0"
    }
    null = {
      source  = "hashicorp/null"
      version = "3.2.1"
    }
  }
}

# Configure the libvirt provider
provider "libvirt" {
  uri = var.libvirt_uri
}

# Configure the PowerDNS provider
provider "powerdns" {
  api_key    = var.pdns_api_key
  server_url = var.pdns_server_url
}

# We fetch the latest ubuntu release image from their mirrors
resource "libvirt_volume" "ubuntu_qcow2" {
  name   = "${var.prefix}-ubuntu.qcow2"
  pool   = var.libvirt_volume_pool
  source = var.libvirt_volume_source
  format = "qcow2"
}

module "cluster" {
  source = "git::https://gitlab.com/2stacks/terraform-libvirt-ubuntu.git//.?ref=v1.2.1"

  count = var.guest_count

  hostname            = format("${var.prefix}-%02d", count.index + 1)
  vcpu                = "4"
  memory              = "8192"
  libvirt_volume_pool = var.libvirt_volume_pool
  libvirt_volume_size = 42949672960
  libvirt_volume_id   = libvirt_volume.ubuntu_qcow2.id
  network             = "ovs-network"
  port_group          = "v6-pub"

  # Cloud-Init using No-Cloud, see - https://cloudinit.readthedocs.io/en/latest/topics/datasources/nocloud.html#datasource-nocloud
  user_data = templatefile("${path.module}/templates/cloud_init.yml.tftpl", {
    user_name          = var.user_name
    user_passwd        = var.user_passwd
    ssh_authorized_key = var.ssh_authorized_key
    package_list       = var.package_list
  })
  meta_data = templatefile("${path.module}/templates/meta_data.yml.tftpl", {
    hostname = format("${var.prefix}-%02d", count.index + 1)
  })
  network_config = file("${path.module}/templates/network_config.yml")
}

# Add multiple A records for rke2 endpoint
resource "powerdns_record" "rke2_vip" {
  zone    = "${var.hosted_domain}."
  name    = "${var.prefix}.${var.hosted_domain}."
  type    = "A"
  ttl     = 300
  records = module.cluster[*].ipv4_address
}

# Add A record for each instance
resource "powerdns_record" "rke2_instance" {
  count = var.guest_count

  zone    = "${var.hosted_domain}."
  name    = "${module.cluster[count.index].instance_name}.${var.hosted_domain}."
  type    = "A"
  ttl     = 300
  records = [module.cluster[count.index].ipv4_address]
}

# Add CNAME record for Rancher service
resource "powerdns_record" "rancher_cname" {
  zone    = "${var.hosted_domain}."
  name    = "rancher.${var.hosted_domain}."
  type    = "CNAME"
  ttl     = 300
  records = [powerdns_record.rke2_vip.name]
}

# Provision instances with local-exec
resource "null_resource" "bootstrap" {
  # Bootstrap the first node in the cluster
  connection {
    host    = module.cluster[0].ipv4_address
    type    = "ssh"
    agent   = true
    user    = var.user_name
    timeout = "2m"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mkdir -p /etc/rancher/rke2/",
      "sudo chgrp ${var.user_name} /etc/rancher/rke2",
      "sudo chmod 775 /etc/rancher/rke2"
    ]
  }

  provisioner "file" {
    content = templatefile("${path.module}/templates/bootstrap.tftpl", {
      shared_secret = var.rke2_secret,
      vip_fqdn      = "${var.prefix}.${var.hosted_domain}"
    })
    destination = "/etc/rancher/rke2/config.yaml"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo curl -sfL https://get.rke2.io | sudo INSTALL_RKE2_CHANNEL=stable sh -",
      "sudo systemctl enable rke2-server.service",
      "sudo systemctl start rke2-server.service"
    ]
  }

  depends_on = [powerdns_record.rke2_vip, ]
}

# Provision instances with local-exec
resource "null_resource" "cluster" {
  # Changes to any instance of the cluster requires re-provisioning
  triggers = {
    cluster_instance_ids = join(" ", module.cluster[*].ipv4_address)
  }

  # Exclude the node used for bootstrapping
  count = var.guest_count - 1

  connection {
    host    = module.cluster[count.index + 1].ipv4_address
    type    = "ssh"
    agent   = true
    user    = var.user_name
    timeout = "2m"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mkdir -p /etc/rancher/rke2/",
      "sudo chgrp ${var.user_name} /etc/rancher/rke2",
      "sudo chmod 775 /etc/rancher/rke2"
    ]
  }

  provisioner "file" {
    content = templatefile("${path.module}/templates/cluster.tftpl", {
      shared_secret = var.rke2_secret,
      vip_fqdn      = "${var.prefix}.${var.hosted_domain}"
    })
    destination = "/etc/rancher/rke2/config.yaml"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo curl -sfL https://get.rke2.io | sudo INSTALL_RKE2_CHANNEL=stable sh -",
      "sudo systemctl enable rke2-server.service",
      "sudo systemctl start rke2-server.service"
    ]
  }

  depends_on = [powerdns_record.rke2_vip, null_resource.bootstrap]
}

# CloudInit Variables
variable "user_name" {
  description = "Host ssh user name"
  type        = string
  default     = "ubuntu"
}

variable "user_passwd" {
  description = "SSH user console password"
  type        = string
  sensitive   = true
}

variable "ssh_authorized_key" {
  description = "SSH Public Key for user"
  type        = string
}

variable "package_list" {
  description = "List of additional packages to install"
  type        = list(string)
  default = [
    "qemu-guest-agent",
  ]
}

# Libvirt Variables
variable "libvirt_uri" {
  description = "URI of server running libvirtd"
  type        = string
  default     = "qemu:///system"
}

variable "libvirt_volume_source" {
  description = "Volume Image Source"
  type        = string
  default     = "https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64-disk-kvm.img"
}

variable "libvirt_volume_pool" {
  description = "Volume Storage Pool"
  type        = string
  default     = "default"
}

variable "guest_count" {
  description = "Number of Guests to Create"
  type        = number
  default     = 3
}

# PowerDNS Variables
variable "pdns_api_key" {
  description = "The PowerDNS API key. This can also be specified with PDNS_API_KEY environment variable"
  type        = string
}

variable "pdns_server_url" {
  description = "The address of PowerDNS server. This can also be specified with PDNS_SERVER_URL environment variable"
  type        = string
  default     = ""
}

variable "hosted_domain" {
  description = "PowerDNS Zone Name"
  type        = string
  default     = "2stacks.net"
}

variable "prefix" {
  description = "Resources will be prefixed with this to avoid clashing names"
  type        = string
  default     = "rke2"
}

# RKE2 Variables
variable "rke2_secret" {
  description = "Shared secret used to join a server or agent to a cluster [$RKE2_TOKEN]"
  type        = string
}
